FROM openjdk:17-alpine
COPY build/libs/RegisterApp-0.0.1-SNAPSHOT.jar / registerApp/
CMD ["java","-jar","/registerApp/RegisterApp-0.0.1-SNAPSHOT.jar"]