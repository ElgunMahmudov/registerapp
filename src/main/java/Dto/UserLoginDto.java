package Dto;

import lombok.Data;

@Data
public class UserLoginDto {
    private String userName;
    private String password;
}
