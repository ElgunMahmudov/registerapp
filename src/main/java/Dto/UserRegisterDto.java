package Dto;

import lombok.Data;

@Data
public class UserRegisterDto {
    private String userName;
    private String password;
    private String confirmPassword;
    private String mail;
    private String verificationEmail;
    private String verificationCode;

}
