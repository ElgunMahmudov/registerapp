package Service;

import Dto.UserLoginDto;
import Dto.UserPasswordResetDto;
import Dto.UserRegisterDto;
import Entity.User;

public interface MainUserService {
    public void registerUser(UserRegisterDto userRegisterDto);
    public boolean loginUser(UserLoginDto userLoginDto);
    public void resetPassword(UserPasswordResetDto passwordResetDto);
    public User  getOneUserByUserName(String username);
    public User getOneUserByEmail(String email);
    public boolean validatePassword(UserRegisterDto userRegisterDto);
    public boolean validateVerificationEmail(UserPasswordResetDto passwordResetDto);
    public boolean validateVerificationCode(UserPasswordResetDto passwordResetDto);

}
