package Service;

import Dto.UserLoginDto;
import Dto.UserPasswordResetDto;
import Dto.UserRegisterDto;
import Entity.User;
import Repository.UserRepository;
import org.modelmapper.ModelMapper;

import org.springframework.stereotype.Service;

@Service

public class UserService implements MainUserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public UserService(UserRepository repository, ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.userRepository = repository;
    }

    public void registerUser(UserRegisterDto userRegisterDto) {
        User user = modelMapper.map(userRegisterDto, User.class);
        userRepository.save(user);
    }

    @Override
    public boolean loginUser(UserLoginDto userLoginDto) {
        User user = userRepository.findByUserName(userLoginDto.getUserName());
        if (user.getPassword().equals(userLoginDto.getPassword())) {
            return true;
        } else return false;
    }

    @Override
    public void resetPassword(UserPasswordResetDto passwordResetDto) {
        User user = userRepository.findByMail(passwordResetDto.getMail());
        user.setPassword(passwordResetDto.getPassword());
        userRepository.save(user);
    }

    @Override
    public User getOneUserByUserName(String username) {
        return userRepository.findByUserName(username);
    }

    @Override
    public User getOneUserByEmail(String email) {
        return  userRepository.findByMail(email);
    }

    @Override
    public boolean validatePassword(UserRegisterDto UserRegisterDto) {
        if (UserRegisterDto.getPassword().equals(UserRegisterDto.getConfirmPassword())) {
            return true;
        } else return false;

    }

    @Override
    public boolean validateVerificationEmail(UserPasswordResetDto passwordResetDto) {
        User user = userRepository.findByMail(passwordResetDto.getMail());
        if (user.getVerificationEmail().equals(passwordResetDto.getVerificationEmail())) {
            return true;
        } else return false;
    }


    @Override
    public boolean validateVerificationCode(UserPasswordResetDto passwordResetDto) {
        User user = userRepository.findByMail(passwordResetDto.getMail());
        if (user.getVerificationEmail().equals(passwordResetDto.getVerificationEmail())) {
            return true;
        } else return false;
    }


}
